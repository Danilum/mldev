#!/bin/bash

# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

set -eo pipefail

# enable debug
set -x

# ask user for missing required parameters or fail
# 0 - ask always and provide current values
# 1 - ask missing only
# 2 - do not ask, fail
# ASK_MISSING=0

# DVC Google Drive Folder
# DVC_REMOTE_NAME=google_drive_remote
# DVC_REMOTE_GDRIVE=""

# environment vars
# LOG_LEVEL=   - same as logging.*

if [[ ${#LOG_LEVEL} -lt 20 ]]; then
  GIT_TRACE=True
  export GIT_TRACE
fi

expand_template_mldev() {
  GLOBIGNORE=".:.."
  mkdir -p "${1:?}"
  tmp_dir=$(mktemp -d)
  curl "https://gitlab.com/mlrep/${2}/-/archive/master/${2}-1-master.tar.gz" | tar -zx -C "${tmp_dir}"
  src_files=( "${tmp_dir:?}/${2:?}"-master-*/* )
  mv "${src_files[@]}" "${1:?}"
  rm -r "${tmp_dir:?}"
}

expand_template_github() {
  GLOBIGNORE=".:.."
  mkdir -p "${1:?}"
  tmp_dir=$(mktemp -d)
  curl -L --silent "${2}/archive/main.zip" -o "${tmp_dir}/template.zip"
  unzip "${tmp_dir}/template.zip" -d "${tmp_dir}/template-unzip"
  src_dir=( "${tmp_dir:?}/template-unzip"/* )
  src_files=( "${src_dir:?}"/* )
  mv "${src_files[@]}" "${1:?}"
  rm -r "${tmp_dir:?}"
}

get_template() {
  # get template
  SUB='darwin'
  if [[ "$OSTYPE" == *"$SUB"* ]]; then
    dir_name=$(greadlink -f "$1");
    else
      dir_name=$(readlink -f "$1");
  fi
  template_name="$2"
  if [ "$template_name" = "-" ]; then template_name="template-default"; fi
  echo "(mldev) Setting up template <$template_name> to $dir_name"
  if [ -d "$2" ]; then echo "(mldev) Directory $dir_name already exists"; exit 1; fi
  echo "(mldev) Using https://gitlab.com/mlrep/${template_name}"
  read -rp "(mldev) Please specify your new remote url: " git_remote

  if [[ $template_name == "https://github.com"* ]]
  then
    expand_template_github "${dir_name}" "${template_name}"
  else
    expand_template_mldev "${dir_name}" "${template_name}"
  fi

  cd "$dir_name"

  # init git
  git init
  git remote add origin "$git_remote"

  refresh_git

  git add *
  git commit -m "(mldev) Template initial commit"
}

refresh_git() {
  _set_e=false
  if [ -o errexit ]; then
    set +e
    _set_e=true
  fi
  user_name="$(git config user.name)"
  user_email="$(git config user.email)"

  if [ -z "$user_name" ] || [ -z "$user_email" ]; then
    echo "(mldev) Your Git is not configured, user.name or user.email is not set"
    read -eri "$user_name" -p "(mldev) Git username: " user_name
    read -ei "$user_email" -p "(mldev) Git email: " user_email

    git config user.name "$user_name"
    git config user.email "$user_email"
  fi
  if [ "$_set_e" == true ]; then set -e; fi
}

if [ $# -eq 0 ]; then
  echo "(mldev) Usage: $0 <folder> [<template-name>] "; exit 1;
fi

if [ -n "$2" ]; then
  echo "(mldev) Configuring new experiment"
  get_template "$1" "$2"
else
  echo "(mldev) Checking your git config"
  refresh_git
fi

if [ ! -d ".mldev" ]; then
  mkdir .mldev/
fi

if [ -n "$2" ]; then
  echo "(mldev) "
  echo "(mldev) You may want to push your local code to repo ${git_remote}"
  echo "(mldev) Type:"
  echo "(mldev)    cd ${dir_name}"
  echo "(mldev)    git push --set-upstream origin master"
  echo "(mldev)  "
fi